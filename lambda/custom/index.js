/* eslint-disable no-mixed-operators */
/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require("ask-sdk-core");

const STREAMS = [
  {
    token: "mg-radio-1",
    url: "https://stream.zeno.fm/13tbqr42u8quv.m3u",
    metadata: {
      title: "Connect FM 97.1",
      subtitle: "Ye nye wo nam",
      art: {
        sources: [
          {
            contentDescription: "Connect FM 97.1",
            url: "https://3news.com/wp-content/uploads/2021/07/connect-radio-stream-logo.png",
            widthPixels: 512,
            heightPixels: 512,
          },
        ],
      },
      backgroundImage: {
        sources: [
          {
            contentDescription: "Connect FM 97.1",
            url: "https://mediageneralgh.com/wp-content/uploads/2018/07/connect-1024x638.png",
            widthPixels: 1200,
            heightPixels: 800,
          },
        ],
      },
    },
  },
  {
    token: "mg-radio-2",
    url: "https://stream.zeno.fm/e5qwnn42u8quv.m3u",
    metadata: {
      title: "3FM 92.7",
      subtitle: "Fair and Square",
      art: {
        sources: [
          {
            contentDescription: "3FM 92.7",
            url: "https://3news.com/wp-content/uploads/2021/07/3fm-radio-stream-logo.png",
            widthPixels: 512,
            heightPixels: 512,
          },
        ],
      },
      backgroundImage: {
        sources: [
          {
            contentDescription: "3FM 92.7",
            url: "https://mediageneralgh.com/wp-content/uploads/2018/07/3fm-1024x638.png",
            widthPixels: 1200,
            heightPixels: 800,
          },
        ],
      },
    },
  },
  {
    token: "mg-radio-3",
    url: "https://stream.zeno.fm/hmz5ma42u8quv.m3u",
    metadata: {
      title: "Onua FM 95.1",
      subtitle: "Ye dwene wo ho",
      art: {
        sources: [
          {
            contentDescription: "Onua FM 95.1",
            url: "https://3news.com/wp-content/uploads/2021/07/onuafm-radio-stream-logo.png",
            widthPixels: 512,
            heightPixels: 512,
          },
        ],
      },
      backgroundImage: {
        sources: [
          {
            contentDescription: "Onua FM 95.1",
            url: "https://mediageneralgh.com/wp-content/uploads/2018/07/onua-1024x638.png",
            widthPixels: 1200,
            heightPixels: 800,
          },
        ],
      },
    },
  },
  {
    token: "mg-radio-4",
    url: "https://stream.zeno.fm/h6y04t42u8quv.m3u",
    metadata: {
      title: "Akoma FM 87.9",
      subtitle: "Wo da yakoma so",
      art: {
        sources: [
          {
            contentDescription: "Akoma FM 87.9",
            url: "https://3news.com/wp-content/uploads/2021/07/akoma-radio-stream-logo.png",
            widthPixels: 512,
            heightPixels: 512,
          },
        ],
      },
      backgroundImage: {
        sources: [
          {
            contentDescription: "Akoma FM 87.9",
            url: "https://mediageneralgh.com/wp-content/uploads/2022/03/AKOMA-FM-LOGO-copy-copy-1024x631.jpg",
            widthPixels: 1200,
            heightPixels: 800,
          },
        ],
      },
    },
  },
];

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "LaunchRequest"
    );
  },
  handle(handlerInput) {
    const speakOutput =
      "Welcome to the media general ghana skill, say listen life to play THREE FM, play CONNECT FM, play ONUA FM, or play AKOMA FM";

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};

const PlayStreamIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "LaunchRequest" ||
      (handlerInput.requestEnvelope.request.type === "IntentRequest" &&
        (handlerInput.requestEnvelope.request.intent.name ===
          "PlayStreamIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.ResumeIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.LoopOnIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.NextIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.PreviousIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.RepeatIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.ShuffleOnIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.StartOverIntent"))
    );
  },
  handle(handlerInput) {
    const stream = STREAMS[0];

    handlerInput.responseBuilder
      .speak(`now playing ${stream.metadata.title}`)
      .addAudioPlayerPlayDirective(
        "REPLACE_ALL",
        stream.url,
        stream.token,
        0,
        null,
        stream.metadata
      );

    return handlerInput.responseBuilder.getResponse();
  },
};

const PlayThreeFMIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "LaunchRequest" ||
      (handlerInput.requestEnvelope.request.type === "IntentRequest" &&
        (handlerInput.requestEnvelope.request.intent.name ===
          "PlayThreeFMIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.ResumeIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.LoopOnIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.NextIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.PreviousIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.RepeatIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.ShuffleOnIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.StartOverIntent"))
    );
  },
  handle(handlerInput) {
    const stream = STREAMS[1];

    handlerInput.responseBuilder
      .speak(`now playing ${stream.metadata.title}`)
      .addAudioPlayerPlayDirective(
        "REPLACE_ALL",
        stream.url,
        stream.token,
        0,
        null,
        stream.metadata
      );

    return handlerInput.responseBuilder.getResponse();
  },
};

const PlayOnuaFMIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "LaunchRequest" ||
      (handlerInput.requestEnvelope.request.type === "IntentRequest" &&
        (handlerInput.requestEnvelope.request.intent.name ===
          "PlayOnuaFMIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.ResumeIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.LoopOnIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.NextIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.PreviousIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.RepeatIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.ShuffleOnIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.StartOverIntent"))
    );
  },
  handle(handlerInput) {
    const stream = STREAMS[2];

    handlerInput.responseBuilder
      .speak(`now playing ${stream.metadata.title}`)
      .addAudioPlayerPlayDirective(
        "REPLACE_ALL",
        stream.url,
        stream.token,
        0,
        null,
        stream.metadata
      );

    return handlerInput.responseBuilder.getResponse();
  },
};

const PlayAkomaFMIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "LaunchRequest" ||
      (handlerInput.requestEnvelope.request.type === "IntentRequest" &&
        (handlerInput.requestEnvelope.request.intent.name ===
          "PlayAkomaFMIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.ResumeIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.LoopOnIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.NextIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.PreviousIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.RepeatIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.ShuffleOnIntent" ||
          handlerInput.requestEnvelope.request.intent.name ===
            "AMAZON.StartOverIntent"))
    );
  },
  handle(handlerInput) {
    const stream = STREAMS[3];

    handlerInput.responseBuilder
      .speak(`now playing ${stream.metadata.title}`)
      .addAudioPlayerPlayDirective(
        "REPLACE_ALL",
        stream.url,
        stream.token,
        0,
        null,
        stream.metadata
      );

    return handlerInput.responseBuilder.getResponse();
  },
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "AMAZON.HelpIntent"
    );
  },
  handle(handlerInput) {
    const speechText =
      "This skill plays an audio stream when it is started. It does not have any additional functionality.";

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .getResponse();
  },
};

const AboutIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "AboutIntent"
    );
  },
  handle(handlerInput) {
    const speechText =
      "This is Media General Ghana skill that was built to play Media General Radio Stations.";

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .getResponse();
  },
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      (handlerInput.requestEnvelope.request.intent.name ===
        "AMAZON.StopIntent" ||
        handlerInput.requestEnvelope.request.intent.name ===
          "AMAZON.PauseIntent" ||
        handlerInput.requestEnvelope.request.intent.name ===
          "AMAZON.CancelIntent" ||
        handlerInput.requestEnvelope.request.intent.name ===
          "AMAZON.LoopOffIntent" ||
        handlerInput.requestEnvelope.request.intent.name ===
          "AMAZON.ShuffleOffIntent")
    );
  },
  handle(handlerInput) {
    handlerInput.responseBuilder
      .addAudioPlayerClearQueueDirective("CLEAR_ALL")
      .addAudioPlayerStopDirective();

    return handlerInput.responseBuilder.getResponse();
  },
};

const PlaybackStoppedIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type ===
        "PlaybackController.PauseCommandIssued" ||
      handlerInput.requestEnvelope.request.type ===
        "AudioPlayer.PlaybackStopped"
    );
  },
  handle(handlerInput) {
    handlerInput.responseBuilder
      .addAudioPlayerClearQueueDirective("CLEAR_ALL")
      .addAudioPlayerStopDirective();

    return handlerInput.responseBuilder.getResponse();
  },
};

const PlaybackStartedIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type ===
      "AudioPlayer.PlaybackStarted"
    );
  },
  handle(handlerInput) {
    handlerInput.responseBuilder.addAudioPlayerClearQueueDirective(
      "CLEAR_ENQUEUED"
    );

    return handlerInput.responseBuilder.getResponse();
  },
};

/* *
 * FallbackIntent triggers when a customer says something that doesn’t map to any intents in your skill
 * It must also be defined in the language model (if the locale supports it)
 * This handler can be safely added but will be ingnored in locales that do not support it yet
 * */
const FallbackIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) ===
        "AMAZON.FallbackIntent"
    );
  },
  handle(handlerInput) {
    const speakOutput = "Sorry, I don't know about that. Please try again.";

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === "SessionEndedRequest";
  },
  handle(handlerInput) {
    console.log(
      `Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`
    );

    return handlerInput.responseBuilder.getResponse();
  },
};

const ExceptionEncounteredRequestHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type ===
      "System.ExceptionEncountered"
    );
  },
  handle(handlerInput) {
    console.log(
      `Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`
    );

    return true;
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);
    console.log(handlerInput.requestEnvelope.request.type);
    return handlerInput.responseBuilder.getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    PlayStreamIntentHandler,
    PlayThreeFMIntentHandler,
    PlayOnuaFMIntentHandler,
    PlayAkomaFMIntentHandler,
    PlaybackStartedIntentHandler,
    CancelAndStopIntentHandler,
    PlaybackStoppedIntentHandler,
    AboutIntentHandler,
    HelpIntentHandler,
    ExceptionEncounteredRequestHandler,
    FallbackIntentHandler,
    SessionEndedRequestHandler
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
